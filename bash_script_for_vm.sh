
# Update the repositories
sudo apt update
# Install nginx
sudo apt install nginx
# Start nginx
sudo systemctl start nginx
# Replace nginx index.html to new index.html
sudo mv ~/Nicole_website/index.html /var/www/html/index.nginx-debian.html
# Restart nginx
sudo systemctl restart nginx 