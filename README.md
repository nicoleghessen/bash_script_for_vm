



### How to send Bash script & website content

These commands need to be ran from ubuntu_bash_script folder as there isn't a full path. 
```bash
$ scp -i ~/.ssh/ch9_shared.pem bash_script_for_vm.sh ubuntu@34.255.200.179:~/
```


### After you've sent the Bash script you need to send the Nicole_website folder
```bash
$ scp -i ~/.ssh/ch9_shared.pem -r Nicole_website ubuntu@34.255.200.179:~/

```
### After moved file and folder, we need to run bash script
```bash
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.255.200.179 ls 

# Running the bash script:

ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.255.200.179 bash bash_script_for_vm.sh 
```